package com.test;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;

public class ProductMasterConfigurationActionImpl extends
		DefaultConfigurationAction {

	public void processAction(PortletConfig config,
			ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {

	}

	public String render(PortletConfig config, RenderRequest renderRequest,
			RenderResponse renderResponse) throws Exception {
		return "/jsp/configuration.jsp";
	}
}