package com.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.json.simple.JSONValue;

import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ExcelPortlet
 */
public class ExcelPortlet extends MVCPortlet {
	// after save
	public void updateTest(ActionRequest actionRequest,
			ActionResponse actionResponset) throws PortletException,
			IOException {

		String realPath = getPortletContext().getRealPath("/");
		System.out.println(realPath);
		byte[] bytes = null;
		String header = "0";
		String firstrow = "0";
		String firstcol = "0";
		String sheet = "0";

		try {

			UploadPortletRequest uploadRequest = PortalUtil
					.getUploadPortletRequest(actionRequest);
			String sourceFileName = uploadRequest.getFileName("fileName");
			System.out.println(sourceFileName);
			File file = uploadRequest.getFile("fileName");

			if (uploadRequest.getParameter("headerrow").toString()
					.compareTo("") != 0) {
				header = uploadRequest.getParameter("headerrow");
			}
			System.out.println(header);
			if (uploadRequest.getParameter("datarow").toString().compareTo("") != 0) {
				firstrow = uploadRequest.getParameter("datarow");
			}
			System.out.println(firstrow);
			if (uploadRequest.getParameter("datacolumn").toString()
					.compareTo("") != 0) {
				firstcol = uploadRequest.getParameter("datacolumn");
			}
			System.out.println(firstcol);
			if (uploadRequest.getParameter("sheet").toString().compareTo("") != 0) {
				sheet = uploadRequest.getParameter("sheet");
			}
			System.out.println(sheet);

			try {
				bytes = FileUtil.getBytes(file);
			} catch (IOException e2) {
				e2.printStackTrace();
			}

			if ((bytes != null) && (bytes.length > 0)) {
				System.out.println(bytes);
				try {
					System.out.println("try");
					File temp = new File(realPath + "html/uploads/"
							+ "temp.xlsx");
					file.renameTo(temp);
					System.out.println(temp.getPath());
					FileInputStream fileInputStream = new FileInputStream(
							realPath + "html/uploads/" + "temp.xlsx");

					fileInputStream.read(bytes);

					fileInputStream.close();
					excelProcess(temp, header, firstrow, firstcol, sheet);
					temp.delete();
				} catch (FileNotFoundException e) {
					System.out.println("File Not Found.");
					e.printStackTrace();
				} catch (IOException e1) {
					System.out.println("Error Reading The File.");
				}
			}

		} catch (Exception e) {
			System.out.println("Exception::::" + e.getMessage());
			SessionMessages.add(actionRequest, "error");
		}

	}

	public void excelProcess(File file, String header, String row,
			String column, String sheet) throws java.io.IOException {
		try {
			int head = Integer.parseInt(header);
			if (head > 0) {
				head--;
			}
			int row1;
			if (Integer.parseInt(row) > head) {
				row1 = Integer.parseInt(row) - 1;
			} else {
				row1 = head + 1;
			}
			System.out.println(row1);
			int col1 = Integer.parseInt(column);
			// Creating Input Stream
			FileInputStream myInput = new FileInputStream(file);

			// Create a workbook using the File System
			org.apache.poi.xssf.usermodel.XSSFWorkbook myWorkBook = new org.apache.poi.xssf.usermodel.XSSFWorkbook(
					myInput);

			// Get the first sheet from workbook
			XSSFSheet mySheet;
			if (sheet != "0") {
				mySheet = myWorkBook.getSheetAt(Integer.parseInt(sheet) - 1);
			} else
				mySheet = myWorkBook.getSheetAt(0);

			int rowcount = 0;
			LinkedList<Map<String, Comparable>> list = new LinkedList<Map<String, Comparable>>();
			LinkedList hlist = new LinkedList();
			LinkedList<String> headerText = new LinkedList<String>();
			String json = "";
			String hjson = "";
			/** We now need something to iterate through the cells. **/
			Iterator<org.apache.poi.ss.usermodel.Row> rowIter = mySheet
					.rowIterator();
			while (rowIter.hasNext()) {
				Map<String, Comparable> map = new LinkedHashMap<String, Comparable>();
				Map hmap = new LinkedHashMap();
				XSSFRow myRow = (XSSFRow) rowIter.next();

				Iterator<Cell> cellIter = myRow.cellIterator();

				while (cellIter.hasNext()) {

					XSSFCell myCell = (XSSFCell) cellIter.next();

					System.out.println("Cell column index: "
							+ myCell.getColumnIndex());
					// get cell type
					System.out.println("Cell Type: " + myCell.getCellType());

					// get cell value
					switch (myCell.getCellType()) {
					case XSSFCell.CELL_TYPE_NUMERIC:
						System.out.println("Cell Value: "
								+ myCell.getNumericCellValue());
						if (rowcount == head) {
							myCell.setCellType(Cell.CELL_TYPE_STRING);
							headerText.add(myCell.getStringCellValue());
							hlist.add(myCell.getStringCellValue());
						} else if (rowcount >= row1) {
							map.put(headerText.get(myCell.getColumnIndex()),
									myCell.getNumericCellValue());
							System.out.println(map.toString());
						}
						break;
					case XSSFCell.CELL_TYPE_STRING:
						System.out.println("Cell Value: "
								+ myCell.getStringCellValue());
						if (rowcount == head) {
							headerText.add(myCell.getStringCellValue());
							hlist.add(myCell.getStringCellValue());
						} else if (rowcount >= row1) {
							map.put(headerText.get(myCell.getColumnIndex()),
									myCell.getStringCellValue());
							System.out.println(map.toString());
						}
						break;
					default:
						System.out.println("Cell Value: "
								+ myCell.getRawValue());
						if (rowcount == head) {
							headerText.add(myCell.getRawValue());
							hlist.add(myCell.getRawValue());
						} else if (rowcount >= row1) {
							map.put(headerText.get(myCell.getColumnIndex()),
									myCell.getRawValue());
						}
						break;
					}
					System.out.println("---");

					// if (myCell.getColumnIndex() == 0
					// && !myCell.getStringCellValue().trim().equals("")
					// && !myCell.getStringCellValue().trim()
					// .equals("Item Number")) {
					// }

				}

				System.out.println(headerText.toString());
				if (rowcount >= row1) {
					list.add(map);
				}
				rowcount++;
			}

			json = JSONValue.toJSONString(list);
			hjson = JSONValue.toJSONString(hlist);
			String realPath = getPortletContext().getRealPath("/");
			FileWriter jsFile = null;
			FileWriter jsHeadFile = null;
			try {
				jsFile = new FileWriter(realPath + "html/uploads/"
						+ "temp.json");
				jsHeadFile = new FileWriter(realPath + "html/uploads/"
						+ "headertemp.json");

				jsFile.write(json.toString());
				jsHeadFile.write(hjson.toString());
				System.out
						.println("Successfully Copied JSON Object to File...");
				System.out.println("\nJSON Object: " + json + hjson);

			} catch (IOException e) {
				e.printStackTrace();

			} finally {
				jsFile.flush();
				jsFile.close();
				jsHeadFile.flush();
				jsHeadFile.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}