<%@page import="javax.portlet.PortletURL"%>
<%@page import="javax.portlet.ActionRequest"%>

<%@ include file="/html/excel/init.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%-- <%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %> --%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Insert title here</title>
</head>
<body>
	<%
		PortletURL updateTestURL = renderResponse.createActionURL();
	updateTestURL.setParameter(
	ActionRequest.ACTION_NAME, "updateTest");
	%>


	<aui:form name="fm" method="POST"
		action="<%=updateTestURL.toString()%>" enctype="multipart/form-data">
		<aui:input type="file" name="fileName" label="Fájl:" size="89000">
			<aui:validator name="required"
				errorMessage="Csak xlsx formátumú fájl tölthető fel!" />
			<aui:validator name="acceptFiles">'xlsx'</aui:validator>
		</aui:input>
		<aui:input name="headerrow"
			label="Fejléc sorszáma: (alapértelmezésben az első)">
			<aui:validator name="digits" errorMessage="Csak számot adhat meg!" />
		</aui:input>
		<aui:input name="datarow"
			label="Az első adatsor sorszáma: (alapértelmezésben a második)">
			<aui:validator name="digits" errorMessage="Csak számot adhat meg!" />
		</aui:input>
		<aui:input name="datacolumn" label="Az első adatoszlop sorszáma: (alapértelmezésben az első)">
			<aui:validator name="digits"  errorMessage="Csak számot adhat meg!"  />
		</aui:input>
		<aui:input name="sheet" label="A munkafüzet száma: (alapértelmezésben az első)">
			<aui:validator name="digits"  errorMessage="Csak számot adhat meg!"  />
		</aui:input>
		<aui:button type="submit" value="Mentés" />
	</aui:form>

	<a href="<portlet:renderURL/>">&laquo; Vissza</a>
</body>
</html>